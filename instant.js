$(document).ready(function(){
	clear_all();
	var timer;
	
	$('#edit-instant').keyup(function() {
		trigger();
	});
	$('#edit-instant').change(function() {
		trigger();
	});

	function trigger() {
		clearTimeout(timer);
		if ($('#edit-instant').val() != '') {
			timer = setTimeout("search()", 400);
		}
		else {
			
			clear_all();
		}
	}
});

function search() {
	var q = ''
	if ($('#autocomplete ul li:first div').length > 0) {
		q = $('#autocomplete ul li:first div').text();
	}
	else {
		q = $('#edit-instant').val();
	}
	
	$.post("/instant/ajax", {'search': q}, function(data) {
		//@TODO: Find a better way to parse search results.
		var results = $(data).find('.search-results');
		$('#result').html(results);
	});

}



function clear_all() {

	$('#result').html('');
	$('#edit-instant').attr('value', '');

}

